@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">
                <img src="{{asset('images/profil.jpg')}}" alt="" width="50px" height="50px" style="border-radius:100%">
                {{Auth::user()->name}}
                </div>
        </div>
                
         
            <a href="#newpost" data-toggle="modal">Add New Post</a><br>
            <a href="#profil" data-toggle="modal">Edit Profil</a>
        </div>
    </div>
</div>

<!-- Modal Edit New Post-->
<div id="newpost" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content Edit new post-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit New Post</h4>
      </div>
      <div class="modal-body">

    <form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group" >
            <p>Post Caption</p>
            <textarea name="caption" class="form-control"></textarea>
        </div>
        <div class="form-group" >
            <p>Post Image</p>
            <input type="file" name="image" class="form-control">
        </div>
        <div class="form-group" >
            <p>Like</p>
            <textarea name="like" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-info btn-block">Save</button>
    </form>

  </div>
</div>
</div>
</div>

<hr>
<div class="card-header">
    <div class="col-md-12">
    <div class="panel panel-default">
    <div class="panel-body">
    
    Instagram
    

    </div>
    </div> 
    </div>
   

    @foreach($posts as $post)
    <div class="col-md-4">
    <div class="panel panel-default">
    <div class="panel-body">
    <div class="show_image"><a href="#{{$post->id}}" data-toggle="modal"><img src="{{asset('images/'.$post->image)}}"></a>
    </div>
    </div>

    <div class="post-footer">
    <div class="button-footer">
    <a class="btn btn-default btn-xs" href="#"><i class="fa fa-comment"></i> Comment</a>
    <span class="btn btn-default btn-xs">2</span>
    <span class="btn btn-default btn-xs"><i class="fa fa-thumbs-up"></i></span>
    <span class="btn btn-default btn-xs">2</span>
    </div>
    </div>
    </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="{{$post->id}}">
        <div class="modal-content">
            <div class="modal-body">
            <div class="show_image"><a href=""><img src="{{asset('images/'.$post->image)}}"></a>
            </div>
            </div>
            </div>
            </div>
     @endforeach
     
</div> <!-- end container -->


@endsection

<style type="text/css"  >
.show_image img{
    width: 100%;
    height: 30%;
}

</style>
