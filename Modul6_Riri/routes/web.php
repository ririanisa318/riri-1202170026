<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['web','auth']], function(){

    Route::get('/', function () {
        $post = App\Post::all();
        return view('welcome')->withPosts($post);
    });

    Route::post('/posts', 'PostsController@store')->name('posts.store');

});
