<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostsController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'caption' => 'required',
            'like' => 'required',
            'image' => 'required',
        ]);

        $post = New Post;
        $post->caption= $request->caption;
        $post->like= $request->like;
        $post->user_id = auth()->id();

        if($request->hasFile('image')){
            $file = $request->file('image');
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $destinationaPath = public_path('/images');
            $file->move($destinationaPath, $fileName);
            $post->image = $fileName;
        }
        $post->save();
        return back()->withMessage('Berhasil Terkirim!');
    }
}
